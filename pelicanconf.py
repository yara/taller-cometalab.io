#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'RGarcia'
SITENAME = 'Taller Cometa'
SITEURL = 'https://taller-cometa.gitlab.io'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Mexico_City'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10


THEME='cometh'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
